from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from django.http import HttpResponse
from django.http import JsonResponse

from .models import *
from .serializers import *

import json
from django.views.decorators.csrf import csrf_exempt

STATUS_OK = "OK"
STATUS_ERR = "ERROR"
STATUS_NOT_FOUND = "404"


class JSONResponse(HttpResponse):
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)




def handle_peliculas(request):
    if request.method == 'GET':
        peliculas = Pelicula.objects.all()

        respuesta = {
            'status': STATUS_OK,
            'data': PeliculaSerializer(peliculas, many=True).data
        }

        return JSONResponse(respuesta)

    else:
        """
        No es ninguno de los metodos que implementamos
        Devolvemos una respuesta informando que no está permitido el metodo
        para no generar un error 500
        """
        return HttpResponse("Método no permitido")

def handle_pelicula(request, identificador):

    if request.method == 'GET':
        
        if Pelicula.objects.filter(id=identificador).exists():
            pelicula = Pelicula.objects.get(id=identificador)

            print(pelicula)
            respuesta = {
                'status': STATUS_OK,
                'data': PeliculaSerializer(pelicula, many=False).data
            }
        else:
            respuesta = {
                'status': STATUS_NOT_FOUND
            }

        """
        # otro metodo posible para manejar que no encuentre el objeto en la bd
        try:
            pelicula = Pelicula.objects.get(id=identificador)
            respuesta = {
                'status': STATUS_OK,
                'data': PeliculaSerializer(pelicula, many=False).data
            }
        except Pelicula.DoesNotExist:
            respuesta = {
                'status': STATUS_NOT_FOUND
            }
        """
    
        return JSONResponse(respuesta)

    else:
        return HttpResponse("Método no permitido")




def handle_directores(request):

    if request.method == 'GET':
        
        directores = Director.objects.all().order_by('apellido')
        respuesta = {
            'status': STATUS_OK,
            'data': DirectorSerializer(directores, many=True).data
        }
        return JSONResponse(respuesta)

    else:
        return HttpResponse("Método no permitido")


@csrf_exempt
def handle_director(request, identificador=None):

    print(request.method)

    if request.method == 'GET':
        
        if Director.objects.filter(id=identificador).exists():
            director = Director.objects.get(id=identificador)

            respuesta = {
                'status': STATUS_OK,
                'data': DirectorSerializer(director, many=False).data
            }
        else:
            respuesta = {
                'status': STATUS_NOT_FOUND
            }
        return JSONResponse(respuesta)

    elif request.method == 'POST':

        data = json.loads(request.body.decode('utf8'))
        
        director = Director()
        director.nombre = data['nombre']
        director.apellido = data['apellido']
        director.nacionalidad = data['nacionalidad']

        try:
            director.save()
            respuesta = {
                'status' : STATUS_OK
            }
        except Exception:
            respuesta = {
                'status' : STATUS_ERR
            }    

        return JSONResponse(respuesta)


    elif request.method == 'DELETE':

        if Director.objects.filter(id=identificador).exists():
            director = Director.objects.get(id=identificador)
            director.delete()

            respuesta = {
                'status': STATUS_OK,
            }
        else:
            respuesta = {
                'status': STATUS_NOT_FOUND
            }
        return JSONResponse(respuesta)

    else:
        return HttpResponse("Método no permitido")