from rest_framework import serializers
from .models import *



class DirectorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Director
        fields = [
            'id',
            'nombre',
            'apellido',
            'nacionalidad',
        ]

class PeliculaSerializer(serializers.ModelSerializer):

    director = DirectorSerializer()

    class Meta:
        model = Pelicula
        fields = [
            'id',
            'nombre',
            'año',
            'genero',
            'duracion',
            'director',
        ]