from django.urls import path, re_path
from .views import *

urlpatterns = [
    path('peliculas', handle_peliculas),
    re_path('pelicula/(?P<identificador>\w+)/$', handle_pelicula),
    path('directores', handle_directores),
    re_path('director/(?P<identificador>\w+)/$', handle_director),
    path('director', handle_director)
]