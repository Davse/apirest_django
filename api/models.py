from django.db import models

# Create your models here.

class Director(models.Model):
    nombre = models.CharField(max_length=100, null=False, default="")
    apellido = models.CharField(max_length=100, null=False, default="")
    nacionalidad = models.CharField(max_length=100, null=False, default="")

class Pelicula(models.Model):
    nombre = models.CharField(max_length=100, null=False, default="")
    año = models.IntegerField(null=True)
    genero = models.CharField(max_length=100, null=False, default="")
    duracion = models.IntegerField(null=True)
    director = models.ForeignKey(Director, models.CASCADE)